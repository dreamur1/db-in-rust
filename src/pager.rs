use std::{fs::File, io::{self, LineWriter, Write, BufReader, BufRead}};

use crate::table::{TableRow, ROWS_PER_PAGE, MAX_TABLE_PAGES};

#[derive(Debug, PartialEq)]
pub enum GetPageStatus {
    FailureToReadFile,
    PageOutOfBounds,
    Success,
}

pub struct GetPageResult {
    pub status: GetPageStatus,
    pub status_message: String,
    pub page: *mut Page,
}

impl GetPageResult {
    pub fn new() -> GetPageResult {
        GetPageResult { status: GetPageStatus::FailureToReadFile, status_message: "---".to_string(), page: std:: ptr:: null_mut() }
    }
}

pub struct Page {
    pub rows: Vec<TableRow>,
}

impl Page {
    pub fn new() -> Page {
        Page {
            rows: Vec:: with_capacity(ROWS_PER_PAGE as usize),
        }
    }
}

pub struct Pager {
    pub file_name: String,
    pub num_rows: u32,
    pub pages: Vec<Page>,

}

impl Pager {    
    pub fn open_file(file_name: &String) -> Pager {
        let open_result: Result<File, io::Error> = File:: options()
                                                            .read(true)
                                                            .write(true)
                                                            .create(true)
                                                            .open(file_name);

        if open_result.is_err() {
            print!("Error opening or creating db file!: {}", & file_name);
        }

        let mut pages: Vec<Page> = Vec:: with_capacity(MAX_TABLE_PAGES as usize);

        for _ in 0..MAX_TABLE_PAGES {
            pages.push(Page:: new());
        }

        let mut pager = Pager {
            file_name: file_name.to_string(),
            num_rows: 0,
            pages,
        };

        pager.num_rows = BufReader:: new(open_result.unwrap()).lines().count() as u32;
        return pager;
    }

    pub fn flush_cache(&mut self, page_number: u32) {
        let rows = &self.pages[page_number as usize].rows;
        let mut stringified_page: String = String:: new();

        for current_row in 0..rows.len() {
            let tmp_id = rows[current_row].id;
            let tmp_username = (&rows[current_row].username).to_string();
            let tmp_email = (&rows[current_row].email).to_string();

            let mut stringified_row: String = [tmp_id.to_string(), tmp_username, tmp_email].join("||");
            stringified_row += "\n";
            stringified_page += &stringified_row;
        }

        let open_result: Result<File, io::Error> = File:: options()
                                                            .write(true)
                                                            .open(&self.file_name);

        if open_result.is_err() {
            print!("Error opening db file while trying to flush cache!\n");
            print!("Details: {}\n", open_result.as_ref().err().unwrap());
            return;
        }

        let write_result = LineWriter:: new(open_result.unwrap()).write_all(stringified_page.as_bytes());

        if write_result.is_err() {
            print!("failure to write to disk!\n");
            print!("Error details: {}\n", write_result.err().unwrap());
        }
    }

    pub fn get_page(&mut self, page_number: u32) -> GetPageResult {
        let mut result: GetPageResult = GetPageResult:: new();
        
        if page_number > MAX_TABLE_PAGES {
            result.status = GetPageStatus:: PageOutOfBounds;
            result.status_message = format!("Error! Tried to fetch a page number out of bounds!\nRequested Page: {}\nMaximum Pages: {}", page_number, MAX_TABLE_PAGES);
            return result;
        }

        if self.pages[page_number as usize].rows.len() == 0 {
            let mut total_pages = self.num_rows / ROWS_PER_PAGE;

            if self.num_rows % ROWS_PER_PAGE > 0 {
                total_pages += 1;
            }

            if page_number <= total_pages {
                let open_result: Result<File, io::Error> = File:: options()
                                                            .read(true)
                                                            .open(&self.file_name);

                if open_result.is_err() {
                    result.status = GetPageStatus:: FailureToReadFile;
                    result.status_message = format!("Error opening db file while trying to get a page of data!\nDetails: {}\n", open_result.as_ref().err().unwrap());
                    return result;
                }

                let page_lines: Vec<String> = BufReader:: new(open_result.unwrap())
                                                                    .lines()
                                                                    .map(|l| {
                                                                        if l.as_ref().is_err() {
                                                                            print!("error reading line: {}\n", l.as_ref().err().unwrap());
                                                                            return "error".to_string();
                                                                        }    
                                                                        return l.as_ref().unwrap().to_string();
                                                                    })
                                                                    .collect();

                self.pages[page_number as usize].rows = page_lines.iter().map(|line| {
                    let values: Vec<&str> = line.split("||").collect();

                    return TableRow {
                        id: values[0].parse:: <u32>().unwrap(),
                        username: values[1].to_string(),
                        email: values[2].to_string(),
                    };
                }).collect();
            }
        }   
        result.status = GetPageStatus:: Success;
        result.page = &mut self.pages[page_number as usize];
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn open_file_should_correctly_init_pager() {
        let test_db_file_name: String = "test.db".to_string();

        let test_pager: Pager = Pager:: open_file(&test_db_file_name);

        assert_eq!(test_pager.file_name, test_db_file_name);
        assert_eq!(test_pager.pages.len(), MAX_TABLE_PAGES as usize);
        assert_eq!(test_pager.pages.len(), test_pager.pages.capacity());
    }

    #[test]
    fn flush_cache_should_correctly_write_to_file() {
        let test_db_file_name: String = "test.db".to_string();
        let mut test_pager: Pager = Pager:: open_file(&test_db_file_name);
        let mut test_page: Page = Page:: new();

        for i in 0..ROWS_PER_PAGE {
            let test_row: TableRow = TableRow { 
                id: i, 
                username: format!("username#{}", i), 
                email: format!("email#{}", i) 
            };
            test_page.rows.push(test_row);
        }

        test_pager.pages[0] = test_page;
        test_pager.flush_cache(0);

        let db_file: File = File:: options().read(true).open(&test_db_file_name).unwrap();
        let lines_count = BufReader:: new(db_file).lines().count();

        assert_eq!(lines_count as u32, ROWS_PER_PAGE)
    }
}