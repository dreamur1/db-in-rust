use std::{io, env, process};

use table::Table;

use crate::{statement::Statement, table::ExecuteStatus};

mod statement;
mod cursor;
mod table;
mod pager;

pub enum CommandStatus {
    Success,
    Unrecognized,
    Exit
}

#[derive(Debug, PartialEq)]
pub enum PrepareStatus {
    Success,
    Unrecognized,
    SyntaxError,
    StringTooLong,
    NegativeId,
    InvalidType,
    InvalidCharacters
}

pub struct PrepareResult {
    status: PrepareStatus,
    status_message: String,
}

impl PrepareResult {
    pub fn new() -> PrepareResult {
        PrepareResult { status: PrepareStatus::Unrecognized, status_message: "---".to_string() }
    }
}

pub fn process_command(buffer: &String, table: &mut Table) -> CommandStatus {
    if buffer.eq(".exit") {
        table.close();
        return CommandStatus:: Exit;
    } else {
        return CommandStatus:: Unrecognized;
    }
}

fn main() {
    let args: Vec<String> = env:: args().collect();
    if args.len() < 2 {
        /* show usage string */
        println!("Must supply a database filename.\n");
        process:: exit(2);
    }

    let db_file_name: String = args[1].clone();

    let mut buffer: String = String:: new();
    let mut table: Table = Table:: open(db_file_name);

    loop {
        buffer.clear();
        print!("-> ");
        io:: Write:: flush(&mut io:: stdout()).unwrap();
        io::stdin().read_line(&mut buffer).unwrap();
        buffer = (*buffer.trim()).to_string();

        if buffer.starts_with(".") == true {
            match process_command(&buffer, &mut table) {
                CommandStatus:: Success => continue,
                CommandStatus:: Unrecognized => {
                    print!("Unrecognized command: {}\n", buffer);
                    continue
                },
                CommandStatus:: Exit => break,
            }
        }

        let mut statement: Statement = Statement:: new();
        let prepare_result: PrepareResult = statement.prepare(&buffer);
        
        match prepare_result.status {
            PrepareStatus:: Success => {},
            _ => {
                println!("{}", prepare_result.status_message);
                continue;
            },
        }

        match statement.process(&mut table) {
            ExecuteStatus:: Success => print!("Statement Executed\n"),
            ExecuteStatus:: TableFull => print!("Error| Table full\n"),
            ExecuteStatus:: Unrecognized => print!("Error| Unrecognized Statement Type: {}\n", buffer),
        }
    }
}
