use std::io::{self};

use crate::{statement::Statement, pager::{GetPageResult, Pager}, cursor::Cursor};

pub const PAGE_SIZE: u32 = 4096;
pub const ROW_SIZE: u32 = 291;
pub const ROWS_PER_PAGE: u32 = PAGE_SIZE / ROW_SIZE;
pub const MAX_TABLE_PAGES: u32 = 100;
pub const MAX_TABLE_ROWS: u32 = ROWS_PER_PAGE * MAX_TABLE_PAGES;

#[derive(Debug, PartialEq)]
pub enum ExecuteStatus {
    TableFull,
    Success,
    Unrecognized
}

pub struct TableRow {
    pub id: u32,
    pub username: String,
    pub email: String,
}

impl TableRow {
    pub fn new() -> TableRow {
        TableRow { id: 0, username: String:: with_capacity(32), email: String:: with_capacity(255) }
    }

    fn clone(& self) -> TableRow {
        let new_val: TableRow = TableRow {
            id: self.id,
            username: self.username.clone(),
            email: self.email.clone(),
        };
        return new_val;
    }
}

pub struct Table {
    pub num_rows: u32,
    pager: Pager,
}

impl Table {
    pub fn open(file_name: String) -> Table {
        let pager: Pager = Pager:: open_file(&file_name);

        Table {
            num_rows: pager.num_rows,
            pager,
        }
    }

    pub fn close(&mut self) {
        let num_full_pages: u32 = self.num_rows / ROWS_PER_PAGE;

        for i in 0..num_full_pages {
            self.pager.flush_cache(i);
        }

        let num_additional_rows: u32 = self.num_rows % ROWS_PER_PAGE;

        if num_additional_rows > 0 {
            self.pager.flush_cache(num_full_pages);
        }
    }

    pub fn get_page(&mut self, page_number: u32) -> GetPageResult {
        self.pager.get_page(page_number)
    }

    pub fn store(&mut self, data: TableRow) {    
        let mut cursor = Cursor:: at_table_end(self);
        let target_row: &mut TableRow = cursor.value();

        target_row.id = data.id;
        target_row.username.clone_from(&data.username);
        target_row.email.clone_from(&data.email);     
    }
    
    pub fn insert(&mut self, statement: &Statement) -> ExecuteStatus {
        if self.num_rows >= MAX_TABLE_ROWS {
            return ExecuteStatus:: TableFull;
        }
        self.store(statement.insert_data.clone());
        self.num_rows += 1;
        return ExecuteStatus:: Success;
    }

    pub fn select(&mut self, _statement: &Statement) -> ExecuteStatus {
        println!("id\t| username\t| email");
        let mut cursor = Cursor:: at_table_start(self);
        while !cursor.end_of_table {
            let row = cursor.value();
            println!("{}\t| {}\t| {}", row.id, row.username, row.email);
            cursor.advance();
        }
        io:: Write:: flush(&mut io:: stdout()).unwrap();
        return ExecuteStatus:: Success;
    }
}

#[cfg(test)]
mod tests {
    use std::borrow::BorrowMut;

    use super::*;
    
    #[test]
    fn store_should_correctly_save_values_in_memory() {
        let mut test_table: Table = Table:: open("test.db".to_string());
        let mut test_data: TableRow = TableRow:: new();
        test_data.id = 34;
        test_data.email = "email".to_string();
        test_data.username = "potato".to_string();

        test_table.store(test_data.clone());

        let mut test_cursor = Cursor:: at_table_end(test_table.borrow_mut());
        let stored_value: &TableRow = test_cursor.value();

        assert_eq!(stored_value.id, test_data.id);
        assert_eq!(stored_value.username, test_data.username);
        assert_eq!(stored_value.email, test_data.email);
    }

    #[test]
    fn insert_should_return_table_full_status_when_appropriate() {
        let mut test_table: Table = Table:: open("test.db".to_string());

        for i in 0..MAX_TABLE_ROWS {
            let mut test_statement: Statement = Statement:: new();
            let test_row: TableRow = TableRow { id: i, username: "potato".to_string(), email: "email".to_string()};
            test_statement.insert_data = test_row;
            test_table.insert(&test_statement);
        }

        let mut test_statement: Statement = Statement:: new();
        let test_row: TableRow = TableRow { id: MAX_TABLE_ROWS+1, username: "potato".to_string(), email: "email".to_string()};
        test_statement.insert_data = test_row;
        let result_status: ExecuteStatus = test_table.insert(&test_statement);

        assert_eq!(result_status, ExecuteStatus:: TableFull);
        assert_eq!(test_table.pager.pages.len(), MAX_TABLE_PAGES as usize);
        assert_eq!(test_table.pager.pages[(MAX_TABLE_PAGES-1) as usize].rows.len(), ROWS_PER_PAGE as usize);
    }
}