use crate::{ PrepareResult, PrepareStatus};
use crate:: table::{ TableRow, Table, ExecuteStatus};

#[derive(Debug, PartialEq)]
pub enum StatementType {
    Select,
    Insert,
    Unrecognized
}

pub struct Statement {
    statment_type: StatementType,
    pub insert_data: TableRow,
}

impl Statement {
    pub fn new() -> Statement {
        Statement {
            statment_type: StatementType:: Unrecognized,
            insert_data: TableRow:: new()
        }
    }

    pub fn process(&self, table: &mut Table) -> ExecuteStatus {
        match self.statment_type {
            StatementType::Insert => table.insert(self),
            StatementType::Select => table.select(self),
            StatementType::Unrecognized => ExecuteStatus:: Unrecognized,
        }
    }

    pub fn prepare(&mut self, buffer: &String) -> PrepareResult {
        let mut result: PrepareResult = PrepareResult:: new();
        if buffer.starts_with("insert") {
            self.statment_type = StatementType:: Insert;
            let insert_args: Vec<&str> = buffer.split(' ').collect();

            result = Self:: sanitize_for_insert(&insert_args);

            if result.status != PrepareStatus:: Success {
                return result;
            }

            self.insert_data.id = insert_args[1].parse::<u32>().unwrap();
            self.insert_data.username = insert_args[2].to_string();
            self.insert_data.email = insert_args[3].to_string();
        } else if buffer.starts_with("select") {
            self.statment_type = StatementType:: Select;
            result.status = PrepareStatus:: Success;
        }
        result.status_message = format!("Unrecognized statment: {}", buffer);
        return result
    }

    fn sanitize_for_insert(args: & Vec<&str>) -> PrepareResult { 
        let mut prepare_result: PrepareResult = PrepareResult { status: PrepareStatus::Success, status_message: " ".to_string() };
        let insert_format_reminder: &str = "\nInsert Format: 'insert <id: u32> <username: String> <email: String>'\n";
        if args.len() != 4 {
           prepare_result.status = PrepareStatus:: SyntaxError;
           prepare_result.status_message = format!("Incorrect args count. Expected 3, but recieved {}.", args.len()-1);
           prepare_result.status_message += insert_format_reminder;
           return prepare_result;
        } 

        let id_result: Result<i32, std::num::ParseIntError> = args[1].parse::<i32>();
        let username_result: Result<u32, std::num::ParseIntError> = args[2].parse::<u32>();
        let email_result: Result<u32, std::num::ParseIntError> = args[3].parse::<u32>();

        if id_result.is_err() {
            prepare_result.status = PrepareStatus:: InvalidType;
            prepare_result.status_message = "Invalid type provided for field: Id\nExpected 'u32' type but received 'String' type.".to_string();
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if username_result.is_ok() {
            prepare_result.status = PrepareStatus:: InvalidType;
            prepare_result.status_message = "Invalid type provided for field: Username\nExpected 'String' type but received integer type.".to_string();
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if email_result.is_ok() {
            prepare_result.status = PrepareStatus:: InvalidType;
            prepare_result.status_message = "Invalid type provided for field: Email\nExpected 'String' type but received integer type.".to_string();
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if id_result.as_ref().unwrap() < &0 {
            prepare_result.status = PrepareStatus:: NegativeId;
            prepare_result.status_message = format!("Cannot insert a record with a negative id!\nId received: {}", id_result.as_ref().unwrap());
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if args[2].len() > 32 {
            prepare_result.status = PrepareStatus:: StringTooLong;
            prepare_result.status_message = format!("The username provided is too long!\nMaximum length: 32\tReceived length: {}\nUsername: {}", args[2].len(), args[2]);
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if args[3].len() > 255 {
            prepare_result.status = PrepareStatus:: StringTooLong;
            prepare_result.status_message = format!("The email provided is too long!\nMaximum length: 255\tReceived length: {}\n\nEmail: {}", args[3].len(), args[3]);
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if args[2].chars().any(|c| !c.is_alphanumeric()) {
            prepare_result.status = PrepareStatus:: InvalidCharacters;
            prepare_result.status_message = "The username provided contains non-alphanumeric characters!".to_string();
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }

        if args[3].chars().any(|c| !c.is_alphanumeric() && (c != '.' && c != '@')) {
            prepare_result.status = PrepareStatus:: InvalidCharacters;
            prepare_result.status_message = "The email provided contains invalid characters!\nOnly alphanumeric characters and '&' or '@' are valid!".to_string();
            prepare_result.status_message += insert_format_reminder;
            return prepare_result;
        }
        
        return prepare_result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn sanitize_for_insert_should_set_correct_prepare_status() {
        let correct_syntax: Vec<&str> = ["insert", "1", "test", "email.com"].to_vec();
        let bad_id_type: Vec<&str> = ["insert", "one", "test", "email.com"].to_vec();
        let id_out_of_range: Vec<&str> = ["insert", "-1", "test", "email.com"].to_vec();
        let bad_username_type: Vec<&str> = ["insert", "1", "33", "email.com"].to_vec();
        let bad_email_type: Vec<&str> = ["insert", "1", "test", "646"].to_vec();
        let incorrect_args_count: Vec<&str> = ["insert", "1", "test"].to_vec();

        let long_username: String = ["a"; 33].iter().cloned().collect::<String>();
        let long_email: String = ["a"; 256].iter().cloned().collect::<String>();

        let username_too_long: Vec<&str> = ["insert", "1", &long_username, "test"].to_vec();
        let email_too_long: Vec<&str> = ["insert", "1", "test", &long_email].to_vec();

        let username_invalid_chars: Vec<&str> = ["insert", "1", "&a_username", "test"].to_vec();
        let email_invalid_chars: Vec<&str> = ["insert", "1", "username", "sql>injection@turbo.squid!"].to_vec();

        assert_eq!(Statement:: sanitize_for_insert(&correct_syntax).status, PrepareStatus:: Success);
        assert_eq!(Statement:: sanitize_for_insert(&bad_id_type).status, PrepareStatus:: InvalidType);
        assert_eq!(Statement:: sanitize_for_insert(&id_out_of_range).status, PrepareStatus:: NegativeId);
        assert_eq!(Statement:: sanitize_for_insert(&bad_username_type).status, PrepareStatus:: InvalidType);
        assert_eq!(Statement:: sanitize_for_insert(&bad_email_type).status, PrepareStatus:: InvalidType);
        assert_eq!(Statement:: sanitize_for_insert(&incorrect_args_count).status, PrepareStatus:: SyntaxError);
        assert_eq!(Statement:: sanitize_for_insert(&username_too_long).status, PrepareStatus:: StringTooLong);
        assert_eq!(Statement:: sanitize_for_insert(&email_too_long).status, PrepareStatus:: StringTooLong);
        assert_eq!(Statement:: sanitize_for_insert(&username_invalid_chars).status, PrepareStatus:: InvalidCharacters);
        assert_eq!(Statement:: sanitize_for_insert(&email_invalid_chars).status, PrepareStatus:: InvalidCharacters);
    }

    #[test]
    fn prepare_should_set_correct_prepare_status() {
        let good_insert_buffer: String = "insert 1 hey email.com".to_string();
        let select_buffer:  String = "select".to_string();
        let unrecognized_buffer: String = "notta_real_statement".to_string();

        let mut statement: Statement = Statement:: new();

        assert_eq!(statement.prepare(&good_insert_buffer).status, PrepareStatus:: Success);
        assert_eq!(statement.prepare(&select_buffer).status, PrepareStatus:: Success);
        assert_eq!(statement.prepare(&unrecognized_buffer).status, PrepareStatus:: Unrecognized);
    }

    #[test]
    fn prepare_should_hydrate_statement_on_insert() {
        let good_insert_buffer: String = "insert 1 hey email.com".to_string();
        let mut statement: Statement = Statement:: new();

        statement.prepare(&good_insert_buffer);

        assert_eq!(1, statement.insert_data.id);
        assert_eq!("hey", statement.insert_data.username);
        assert_eq!("email.com", statement.insert_data.email);

        assert_eq!(StatementType:: Insert, statement.statment_type);
    }
}