use crate::table::{Table, ROWS_PER_PAGE, TableRow};

pub struct Cursor {
    table: *mut Table,
    row_num: u32, 
    pub end_of_table: bool,
}

impl Cursor {
    pub fn at_table_start(table: *mut Table) -> Cursor {
        Cursor {
            table,
            row_num: 0,
            end_of_table: false
        }
    }

    pub fn at_table_end(table: *mut Table) -> Cursor {
        unsafe {
            Cursor {
                table,
                row_num: (*table).num_rows,
                end_of_table: true
            }
        }
    }

    pub fn advance(&mut self) {
        self.row_num += 1;
        unsafe {
            if self.row_num >= (*self.table).num_rows {
                self.end_of_table = true;
            }
        }
    }

    pub fn value(&mut self) -> &mut TableRow {
        let page_num = self.row_num / ROWS_PER_PAGE;
        unsafe {
            let get_page_result = (*self.table).get_page(page_num);

            let row_offset: usize = (self.row_num % ROWS_PER_PAGE) as usize;
            let row_result = get_page_result.page.as_ref().unwrap().rows.get(row_offset);
            if row_result.is_none() {
                get_page_result.page.as_mut().unwrap().rows.push(TableRow:: new());
            }
            return &mut get_page_result.page.as_mut().unwrap().rows[row_offset];
        }
    }
}